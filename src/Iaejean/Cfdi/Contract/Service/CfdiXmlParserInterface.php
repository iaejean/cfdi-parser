<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Contract\Service;

use Iaejean\Cfdi\Exception\InvalidArgumentException;
use Iaejean\Cfdi\Model\AbstractCfdi;

interface CfdiXmlParserInterface
{
    /**
     * @param string $xmlString
     * @return AbstractCfdi
     * @throws InvalidArgumentException
     */
    public function parseCfdi(string $xmlString): AbstractCfdi;

    /**
     * @param string $xmlString
     * @return string
     * @throws InvalidArgumentException
     */
    public function parseJson(string $xmlString): string;

    /**
     * @param string $xmlString
     * @return \SimpleXMLElement
     * @throws InvalidArgumentException
     */
    public function parseSimpleXmlElement(string $xmlString): \SimpleXMLElement;
}
