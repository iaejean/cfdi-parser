<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Contract\Service;

use Iaejean\Cfdi\Exception\InvalidArgumentException;
use Iaejean\Cfdi\Model\AbstractCfdi;

interface CfdiJsonParserInterface
{
    /**
     * @param string $jsonString
     * @return AbstractCfdi
     * @throws InvalidArgumentException
     */
    public function parseCfdi(string $jsonString): AbstractCfdi;

    /**
     * @param string $jsonString
     * @return string
     * @throws InvalidArgumentException
     */
    public function parseXml(string $jsonString): string;
}
