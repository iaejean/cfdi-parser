<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static V3_3(): CfdiVersionEnum
 * @method static V3_2(): CfdiVersionEnum
 */
class CfdiVersionEnum extends Enum
{
    private const V3_3 = '3.3';
    private const V3_2 = '3.2';
}
