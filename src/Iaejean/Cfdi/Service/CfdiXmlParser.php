<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Service;

use Iaejean\Cfdi\Contract\Service\CfdiXmlParserInterface;
use Iaejean\Cfdi\Exception\InvalidArgumentException;
use Iaejean\Cfdi\Model\AbstractCfdi;
use Iaejean\Cfdi\Model\V3_2\Cfdi as CfdiV3_2;
use Iaejean\Cfdi\Model\V3_3\Cfdi as CfdiV3_3;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CfdiXmlParser implements CfdiXmlParserInterface
{
    protected SerializerInterface $serializer;

    protected ValidatorInterface $validator;

    protected LoggerInterface $logger;

    public function __construct(SerializerInterface $serializer, ValidatorInterface $validator, LoggerInterface $logger)
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->logger = $logger;
    }

    public function parseCfdi(string $xmlString): AbstractCfdi
    {
        self::validateXml($xmlString);
        $xml = simplexml_load_string($xmlString);
        if ($xml->attributes()['version']) {
            return $this->serializer->deserialize($xmlString, CfdiV3_2::class, 'xml');
        }

        return $this->serializer->deserialize($xmlString, CfdiV3_3::class, 'xml');
    }

    public function parseJson(string $xmlString): string
    {
        $cfdi = $this->parseCfdi($xmlString);
        return $this->serializer->serialize($cfdi, 'json');
    }

    public function parseSimpleXmlElement(string $xmlString): \SimpleXMLElement
    {
        self::validateXml($xmlString);

        return simplexml_load_string($xmlString);
    }

    private static function validateXml(string $xml): void
    {
        libxml_use_internal_errors(true);

        $xml = simplexml_load_string($xml);
        if (!$xml) {
            $errors = libxml_get_errors();
            libxml_clear_errors();
            $errorsString = array_reduce($errors, static fn ($carry, $item) => $carry.' '.$item->message, '');

            throw new InvalidArgumentException(sprintf('This is not an valid XML: %s', $errorsString));
        }
    }
}
