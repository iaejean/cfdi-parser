<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Service;

use Iaejean\Cfdi\Contract\Service\CfdiJsonParserInterface;
use Iaejean\Cfdi\Enum\CfdiVersionEnum;
use Iaejean\Cfdi\Exception\InvalidArgumentException;
use Iaejean\Cfdi\Model\AbstractCfdi;
use Iaejean\Cfdi\Model\V3_2\Cfdi as CfdiV3_2;
use Iaejean\Cfdi\Model\V3_3\Cfdi as CfdiV3_3;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CfdiJsonParser implements CfdiJsonParserInterface
{
    protected SerializerInterface $serializer;

    protected ValidatorInterface $validator;

    protected LoggerInterface $logger;

    public function __construct(SerializerInterface $serializer, ValidatorInterface $validator, LoggerInterface $logger)
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->logger = $logger;
    }

    public function parseCfdi(string $jsonString): AbstractCfdi
    {
        self::validateJson($jsonString);
        $cfdiVersion = new CfdiVersionEnum(json_decode($jsonString)->version);
        if ($cfdiVersion->equals(CfdiVersionEnum::V3_2())) {
            return $this->serializer->deserialize($jsonString, CfdiV3_2::class, 'json');
        }

        return $this->serializer->deserialize($jsonString, CfdiV3_3::class, 'json');
    }

    public function parseXml(string $jsonString): string
    {
        $cfdi = $this->parseCfdi($jsonString);
        return $this->serializer->serialize($cfdi, 'xml');
    }

    private static function validateJson(string $json): void
    {
        try {
            json_decode($json, false, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $jsonException) {
            throw new InvalidArgumentException($jsonException->getMessage(), $jsonException->getCode(), $jsonException);
        }
    }
}
