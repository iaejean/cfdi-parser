<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends \Exception
{
    private ?ConstraintViolationListInterface $violations;

    public function __construct(
        string $message,
        ?ConstraintViolationListInterface $violations = null,
        ?int $code = null,
        ?\Exception $previous = null
    ) {
        $this->violations = $violations;
        parent::__construct($message, $code, $previous);
    }

    public function getViolations(): ?ConstraintViolationListInterface
    {
        return $this->violations;
    }
}
