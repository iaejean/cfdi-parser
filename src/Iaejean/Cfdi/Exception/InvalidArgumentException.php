<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Exception;

class InvalidArgumentException extends \InvalidArgumentException
{
}
