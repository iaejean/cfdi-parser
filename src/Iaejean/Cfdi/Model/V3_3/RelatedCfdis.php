<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_3;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

class RelatedCfdis
{
    /**
     * @Serializer\SerializedName("TipoRelacionado")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $relationType = null;

    /**
     * @var null|Collection&RelatedCfdi[]
     * @Serializer\SerializedName("CfdiRelacionado")
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_3\RelatedCfdi>")
     * @Serializer\XmlList(inline=true, entry="cfdi:CfdiRelacionado")
     */
    protected ?Collection $relatedCfdi = null;

    public function __construct()
    {
        $this->relatedCfdi = new ArrayCollection();
    }

    public function getRelationType(): ?string
    {
        return $this->relationType;
    }

    public function setRelationType(?string $relationType): RelatedCfdis
    {
        $this->relationType = $relationType;
        return $this;
    }

    /**
     * @return null|Collection&RelatedCfdi[]
     */
    public function getRelatedCfdi(): ?Collection
    {
        return $this->relatedCfdi;
    }

    /**
     * @param null|Collection&RelatedCfdi[] $relatedCfdi
     * @return RelatedCfdis
     */
    public function setRelatedCfdi(?Collection $relatedCfdi): RelatedCfdis
    {
        $this->relatedCfdi = $relatedCfdi;
        return $this;
    }
}
