<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_3;

use JMS\Serializer\Annotation as Serializer;

class Emitter
{
    /**
     * @Serializer\SerializedName("Rfc")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $rfc;

    /**
     * @Serializer\SerializedName("Nombre")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $name;

    /**
     * @Serializer\SerializedName("RegimenFiscal")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $fiscalRegime;

    public function getRfc(): string
    {
        return $this->rfc;
    }

    public function setRfc(string $rfc): Emitter
    {
        $this->rfc = $rfc;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Emitter
    {
        $this->name = $name;
        return $this;
    }

    public function getFiscalRegime(): string
    {
        return $this->fiscalRegime;
    }

    public function setFiscalRegime(string $fiscalRegime): Emitter
    {
        $this->fiscalRegime = $fiscalRegime;
        return $this;
    }
}
