<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_3;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

class Concept
{
    /**
     * @Serializer\SerializedName("ClaveProdServ")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $code = null;

    /**
     * @Serializer\SerializedName("ClaveUnidad")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $unitCode = null;

    /**
     * @Serializer\SerializedName("Cantidad")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $quantity = null;

    /**
     * @Serializer\SerializedName("NoIdentificacion")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $identificationNumber = null;

    /**
     * @Serializer\SerializedName("Unidad")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $unit = null;

    /**
     * @Serializer\SerializedName("Descripcion")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $description = null;

    /**
     * @Serializer\SerializedName("ValorUnitario")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $unitPrice = null;

    /**
     * @Serializer\SerializedName("Importe")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $amount = null;

    /**
     * @Serializer\SerializedName("Descuento")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $discount = null;

    /**
     * @var Collection&Tax[]|null
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_3\Tax>")
     * @Serializer\SerializedName("Impuestos")
     * @Serializer\XmlList(inline=true, entry="Impuestos", namespace="http://www.sat.gob.mx/cfd/3")
     */
    protected ?Collection $taxes = null;

    /**
     * @var Collection&CustomsInformation[]|null
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_3\CustomsInformation>")
     * @Serializer\SerializedName("InformacionAduanera")
     * @Serializer\XmlList(
     *     inline=true,
     *     entry="InformacionAduanera",
     *     namespace="http://www.sat.gob.mx/cfd/3",
     *     skipWhenEmpty=true
     * )
     */
    protected ?Collection $customsInformation = null;

    /**
     * @var Collection&PredialAccount[]|null
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_3\PredialAccount>")
     * @Serializer\SerializedName("CuentaPredial")
     * @Serializer\XmlList(
     *     inline=true,
     *     entry="CuentaPredial",
     *     namespace="http://www.sat.gob.mx/cfd/3",
     *     skipWhenEmpty=true
     * )
     */
    protected ?Collection $predialAccount = null;

    /**
     * @var Collection&Part[]|null
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_3\Part>")
     * @Serializer\SerializedName("Parte")
     * @Serializer\XmlList(inline=true, entry="Parte", namespace="http://www.sat.gob.mx/cfd/3", skipWhenEmpty=true)
     */
    protected ?Collection $part = null;

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): Concept
    {
        $this->code = $code;
        return $this;
    }

    public function getUnitCode(): ?string
    {
        return $this->unitCode;
    }

    public function setUnitCode(?string $unitCode): Concept
    {
        $this->unitCode = $unitCode;
        return $this;
    }

    public function getIdentificationNumber(): ?string
    {
        return $this->identificationNumber;
    }

    public function setIdentificationNumber(?string $identificationNumber): Concept
    {
        $this->identificationNumber = $identificationNumber;
        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(?float $quantity): Concept
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit(?string $unit): Concept
    {
        $this->unit = $unit;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Concept
    {
        $this->description = $description;
        return $this;
    }

    public function getUnitPrice(): ?float
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(?float $unitPrice): Concept
    {
        $this->unitPrice = $unitPrice;
        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): Concept
    {
        $this->amount = $amount;
        return $this;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(?float $discount): Concept
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @return Collection&Tax[]|null
     */
    public function getTaxes(): ?Collection
    {
        return $this->taxes;
    }

    /**
     * @param Collection&Tax[]|null $taxes
     * @return $this
     */
    public function setTaxes(?Collection $taxes): Concept
    {
        $this->taxes = $taxes;
        return $this;
    }

    /**
     * @return Collection&CustomsInformation[]|null
     */
    public function getCustomsInformation(): ?Collection
    {
        return $this->customsInformation;
    }

    /**
     * @param Collection&CustomsInformation[]|null $customsInformation
     * @return Concept
     */
    public function setCustomsInformation(?Collection $customsInformation)
    {
        $this->customsInformation = $customsInformation;
        return $this;
    }

    /**
     * @return Collection&PredialAccount[]|null
     */
    public function getPredialAccount(): ?Collection
    {
        return $this->predialAccount;
    }

    /**
     * @param Collection&PredialAccount[]|null $predialAccount
     * @return Concept
     */
    public function setPredialAccount(?Collection $predialAccount)
    {
        $this->predialAccount = $predialAccount;
        return $this;
    }

    /**
     * @return Collection&Part[]|null
     */
    public function getPart()
    {
        return $this->part;
    }

    /**
     * @param Collection&Part[]|null $part
     * @return Concept
     */
    public function setPart($part)
    {
        $this->part = $part;
        return $this;
    }
}
