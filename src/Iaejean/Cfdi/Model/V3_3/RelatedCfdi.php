<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_3;

use JMS\Serializer\Annotation as Serializer;

class RelatedCfdi
{
    /**
     * @Serializer\SerializedName("UUID")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $uuid;

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): RelatedCfdi
    {
        $this->uuid = $uuid;
        return $this;
    }
}
