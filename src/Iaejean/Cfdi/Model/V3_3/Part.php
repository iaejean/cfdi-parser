<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_3;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

class Part
{
    /**
     * @Serializer\SerializedName("ClaveProdServ")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $code = null;

    /**
     * @Serializer\SerializedName("Cantidad")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $quantity = null;

    /**
     * @Serializer\SerializedName("NoIdentificacion")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $identificationNumber = null;

    /**
     * @Serializer\SerializedName("Unidad")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $unit = null;

    /**
     * @Serializer\SerializedName("Descripcion")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $description = null;

    /**
     * @Serializer\SerializedName("ValorUnitario")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $unitPrice = null;

    /**
     * @Serializer\SerializedName("Importe")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $amount = null;

    /**
     * @var Collection&CustomsInformation[]|null
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_3\CustomsInformation>")
     * @Serializer\SerializedName("InformacionAduanera")
     * @Serializer\XmlList(inline=true, entry="InformacionAduanera", namespace="http://www.sat.gob.mx/cfd/3")
     */
    protected ?Collection $customsInformation = null;

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getIdentificationNumber(): ?string
    {
        return $this->identificationNumber;
    }

    public function setIdentificationNumber(?string $identificationNumber): Part
    {
        $this->identificationNumber = $identificationNumber;
        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(?float $quantity): Part
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit(?string $unit): Part
    {
        $this->unit = $unit;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Part
    {
        $this->description = $description;
        return $this;
    }

    public function getUnitPrice(): ?float
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(?float $unitPrice): Part
    {
        $this->unitPrice = $unitPrice;
        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): Part
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return Collection&CustomsInformation[]|null
     */
    public function getCustomsInformation(): ?Collection
    {
        return $this->customsInformation;
    }

    /**
     * @param Collection&CustomsInformation[]|null $customsInformation
     * @return Part
     */
    public function setCustomsInformation(?Collection $customsInformation)
    {
        $this->customsInformation = $customsInformation;
        return $this;
    }
}
