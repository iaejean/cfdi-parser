<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_3;

use JMS\Serializer\Annotation as Serializer;

class CustomsInformation
{
    /**
     * @Serializer\SerializedName("NumeroPedimento")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $requestNumber;

    public function getRequestNumber(): string
    {
        return $this->requestNumber;
    }

    public function setRequestNumber(string $requestNumber): CustomsInformation
    {
        $this->requestNumber = $requestNumber;
        return $this;
    }
}
