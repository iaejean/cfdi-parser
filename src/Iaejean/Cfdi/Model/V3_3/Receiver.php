<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_3;

use JMS\Serializer\Annotation as Serializer;

class Receiver
{
    /**
     * @Serializer\SerializedName("Rfc")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $rfc;

    /**
     * @Serializer\SerializedName("Nombre")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $name;

    /**
     * @Serializer\SerializedName("UsoCFDI")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $cfdiUse;

    /**
     * @Serializer\SerializedName("ResidenciaFiscal")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $fiscalAddress = null;

    /**
     * @Serializer\SerializedName("NumRegIdTrib")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $taxPayerId = null;

    public function getRfc(): string
    {
        return $this->rfc;
    }

    public function setRfc(string $rfc): Receiver
    {
        $this->rfc = $rfc;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Receiver
    {
        $this->name = $name;
        return $this;
    }

    public function getCfdiUse(): string
    {
        return $this->cfdiUse;
    }

    public function setCfdiUse(string $cfdiUse): Receiver
    {
        $this->cfdiUse = $cfdiUse;
        return $this;
    }

    public function getFiscalAddress(): ?string
    {
        return $this->fiscalAddress;
    }

    public function setFiscalAddress(?string $fiscalAddress): Receiver
    {
        $this->fiscalAddress = $fiscalAddress;
        return $this;
    }

    public function getTaxPayerId(): ?string
    {
        return $this->taxPayerId;
    }

    public function setTaxPayerId(?string $taxPayerId): Receiver
    {
        $this->taxPayerId = $taxPayerId;
        return $this;
    }
}
