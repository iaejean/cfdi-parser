<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_3;

use JMS\Serializer\Annotation as Serializer;

class Transfer
{
    /**
     * @Serializer\Type("float")
     * @Serializer\SerializedName("Base")
     * @Serializer\XmlAttribute()
     */
    protected ?float $base = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("Impuesto")
     * @Serializer\XmlAttribute()
     */
    protected ?string $tax = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("TipoFactor")
     * @Serializer\XmlAttribute()
     */
    protected ?string $factorType = null;

    /**
     * @Serializer\Type("float")
     * @Serializer\SerializedName("TasaOCuota")
     * @Serializer\XmlAttribute()
     */
    protected ?float $fee = null;

    /**
     * @Serializer\Type("float")
     * @Serializer\SerializedName("Importe")
     * @Serializer\XmlAttribute()
     */
    protected ?float $amount = null;

    public function getBase(): ?float
    {
        return $this->base;
    }

    public function setBase(?float $base): Transfer
    {
        $this->base = $base;
        return $this;
    }

    public function getTax(): ?string
    {
        return $this->tax;
    }

    public function setTax(?string $tax): Transfer
    {
        $this->tax = $tax;
        return $this;
    }

    public function getFactorType(): ?string
    {
        return $this->factorType;
    }

    public function setFactorType(?string $factorType): Transfer
    {
        $this->factorType = $factorType;
        return $this;
    }

    public function getFee(): ?float
    {
        return $this->fee;
    }

    public function setFee(?float $fee): Transfer
    {
        $this->fee = $fee;
        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): Transfer
    {
        $this->amount = $amount;
        return $this;
    }
}
