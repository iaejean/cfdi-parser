<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Iaejean\Cfdi\Enum\CfdiVersionEnum;
use Iaejean\Cfdi\Model\AbstractCfdi;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\XmlNamespace(uri="http://www.sat.gob.mx/cfd/3", prefix="cfdi")
 * @Serializer\XmlNamespace(uri="http://www.w3.org/2001/XMLSchema-instance", prefix="xsi")
 * @Serializer\XmlRoot("cfdi:Comprobante")
 */
class Cfdi extends AbstractCfdi
{
    private const SCHEMA_LOCATION = 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd';

    protected string $version;

    /**
     * @Serializer\SerializedName("serie")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $serie = null;

    /**
     * @Serializer\SerializedName("folio")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $folio = null;

    /**
     * @Serializer\SerializedName("sello")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $seal;

    /**
     * @Serializer\SerializedName("fecha")
     * @Serializer\Type("DateTimeImmutable<'Y-m-d\TH:i:s', 'America/Mexico_City'>")
     * @Serializer\XmlAttribute()
     */
    protected \DateTimeInterface $date;

    /**
     * @Serializer\SerializedName("noCertificado")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $certificateNumber;

    /**
     * @Serializer\SerializedName("certificado")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $certificate;

    /**
     * @Serializer\SerializedName("subTotal")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected float $subtotal;

    /**
     * @Serializer\SerializedName("descuento")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $discount = null;

    /**
     * @Serializer\SerializedName("motivoDescuento")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $discountReason = null;

    /**
     * @Serializer\SerializedName("Moneda")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $currency;

    /**
     * @Serializer\SerializedName("total")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected float $total;

    /**
     * @Serializer\SerializedName("tipoDeComprobante")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $voucherType;

    /**
     * @Serializer\SerializedName("formaDePago")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $payWay;

    /**
     * @Serializer\SerializedName("metodoDePago")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $paymentMethod;

    /**
     * @Serializer\SerializedName("condicionesDePago")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $paymentConditions = null;

    /**
     * @Serializer\SerializedName("LugarExpedicion")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $expeditionPlace;

    /**
     * @Serializer\SerializedName("NumCtaPago")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $paymentAccountNumber = null;

    /**
     * @Serializer\SerializedName("FolioFiscalOrig")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $originalFiscalFolio = null;

    /**
     * @Serializer\SerializedName("SerieFolioFiscalOrig")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $originalFiscalFolioSeries = null;

    /**
     * @Serializer\SerializedName("FechaFolioFiscalOrig")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $originalFiscalFolioDate = null;

    /**
     * @Serializer\SerializedName("MontoFolioFiscalOrig")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $originalFiscalFolioAmount = null;

    /**
     * @Serializer\SerializedName("TipoCambio")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $exchangeRate = null;

    /**
     * @Serializer\Type("Iaejean\Cfdi\Model\V3_2\Emitter")
     * @Serializer\SerializedName("Emisor")
     * @Serializer\XmlElement(namespace="http://www.sat.gob.mx/cfd/3")
     */
    protected Emitter $emitter;

    /**
     * @Serializer\Type("Iaejean\Cfdi\Model\V3_2\Receiver")
     * @Serializer\SerializedName("Receptor")
     * @Serializer\XmlElement(namespace="http://www.sat.gob.mx/cfd/3")
     */
    protected Receiver $receiver;

    /**
     * @var Collection&Concept[]
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_2\Concept>")
     * @Serializer\SerializedName("Conceptos")
     * @Serializer\XmlElement(namespace="http://www.sat.gob.mx/cfd/3")
     * @Serializer\XmlList(inline=false, entry="Concepto", namespace="http://www.sat.gob.mx/cfd/3")
     */
    protected Collection $concepts;

    /**
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_2\Tax>")
     * @Serializer\SerializedName("Impuestos")
     * Serializer\XmlElement(namespace="http://www.sat.gob.mx/cfd/3")
     * @Serializer\XmlList(inline=true, entry="Impuestos", namespace="http://www.sat.gob.mx/cfd/3")
     */
    protected ?Collection $taxes = null;

    /**
     * @Serializer\Type("Iaejean\Cfdi\Model\V3_2\Complement")
     * @Serializer\SerializedName("Complemento")
     * @Serializer\XmlElement(namespace="http://www.sat.gob.mx/cfd/3")
     */
    protected ?Complement $complement;

    public function __construct()
    {
        $this->concepts = new ArrayCollection();
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("xsi:schemaLocation")
     * @Serializer\XmlAttribute()
     */
    public function getSchemaLocation(): string
    {
        return self::SCHEMA_LOCATION;
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    public function getSerie(): ?string
    {
        return $this->serie;
    }

    public function setSerie(?string $serie): Cfdi
    {
        $this->serie = $serie;
        return $this;
    }

    public function getFolio(): ?string
    {
        return $this->folio;
    }

    public function setFolio(?string $folio): Cfdi
    {
        $this->folio = $folio;
        return $this;
    }

    public function getSeal(): string
    {
        return $this->seal;
    }

    public function setSeal(string $seal): Cfdi
    {
        $this->seal = $seal;
        return $this;
    }

    public function getDate(): \DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): Cfdi
    {
        $this->date = $date;
        return $this;
    }

    public function getCertificateNumber(): string
    {
        return $this->certificateNumber;
    }

    public function setCertificateNumber(string $certificateNumber): Cfdi
    {
        $this->certificateNumber = $certificateNumber;
        return $this;
    }

    public function getCertificate(): string
    {
        return $this->certificate;
    }

    public function setCertificate(string $certificate): Cfdi
    {
        $this->certificate = $certificate;
        return $this;
    }

    public function getSubtotal(): float
    {
        return $this->subtotal;
    }

    public function setSubtotal(float $subtotal): Cfdi
    {
        $this->subtotal = $subtotal;
        return $this;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(?float $discount): Cfdi
    {
        $this->discount = $discount;
        return $this;
    }

    public function getDiscountReason(): ?float
    {
        return $this->discountReason;
    }

    public function setDiscountReason(?float $discountReason): Cfdi
    {
        $this->discountReason = $discountReason;
        return $this;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): Cfdi
    {
        $this->currency = $currency;
        return $this;
    }

    public function getTotal(): float
    {
        return $this->total;
    }

    public function setTotal(float $total): Cfdi
    {
        $this->total = $total;
        return $this;
    }

    public function getVoucherType(): string
    {
        return $this->voucherType;
    }

    public function setVoucherType(string $voucherType): Cfdi
    {
        $this->voucherType = $voucherType;
        return $this;
    }

    public function getPayWay(): string
    {
        return $this->payWay;
    }

    public function setPayWay(string $payWay): Cfdi
    {
        $this->payWay = $payWay;
        return $this;
    }

    public function getPaymentMethod(): string
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(string $paymentMethod): Cfdi
    {
        $this->paymentMethod = $paymentMethod;
        return $this;
    }

    public function getPaymentConditions(): ?string
    {
        return $this->paymentConditions;
    }

    public function setPaymentConditions(?string $paymentConditions): Cfdi
    {
        $this->paymentConditions = $paymentConditions;
        return $this;
    }

    public function getExpeditionPlace(): string
    {
        return $this->expeditionPlace;
    }

    public function setExpeditionPlace(string $expeditionPlace): Cfdi
    {
        $this->expeditionPlace = $expeditionPlace;
        return $this;
    }

    public function getPaymentAccountNumber(): ?string
    {
        return $this->paymentAccountNumber;
    }

    public function setPaymentAccountNumber(?string $paymentAccountNumber): Cfdi
    {
        $this->paymentAccountNumber = $paymentAccountNumber;
        return $this;
    }

    public function getOriginalFiscalFolio(): ?string
    {
        return $this->originalFiscalFolio;
    }

    public function setOriginalFiscalFolio(?string $originalFiscalFolio): Cfdi
    {
        $this->originalFiscalFolio = $originalFiscalFolio;
        return $this;
    }

    public function getOriginalFiscalFolioSeries(): ?string
    {
        return $this->originalFiscalFolioSeries;
    }

    public function setOriginalFiscalFolioSeries(?string $originalFiscalFolioSeries): Cfdi
    {
        $this->originalFiscalFolioSeries = $originalFiscalFolioSeries;
        return $this;
    }

    public function getOriginalFiscalFolioDate(): ?string
    {
        return $this->originalFiscalFolioDate;
    }

    public function setOriginalFiscalFolioDate(?string $originalFiscalFolioDate): Cfdi
    {
        $this->originalFiscalFolioDate = $originalFiscalFolioDate;
        return $this;
    }

    public function getOriginalFiscalFolioAmount(): ?float
    {
        return $this->originalFiscalFolioAmount;
    }

    public function setOriginalFiscalFolioAmount(?float $originalFiscalFolioAmount): Cfdi
    {
        $this->originalFiscalFolioAmount = $originalFiscalFolioAmount;
        return $this;
    }

    public function getExchangeRate(): ?float
    {
        return $this->exchangeRate;
    }

    public function setExchangeRate(?float $exchangeRate): Cfdi
    {
        $this->exchangeRate = $exchangeRate;
        return $this;
    }

    public function getEmitter(): Emitter
    {
        return $this->emitter;
    }

    public function setEmitter(Emitter $emitter): Cfdi
    {
        $this->emitter = $emitter;
        return $this;
    }

    public function getReceiver(): Receiver
    {
        return $this->receiver;
    }

    public function setReceiver(Receiver $receiver): Cfdi
    {
        $this->receiver = $receiver;
        return $this;
    }

    public function getComplement(): ?Complement
    {
        return $this->complement;
    }

    public function setComplement(?Complement $complement): Cfdi
    {
        $this->complement = $complement;
        return $this;
    }

    /**
     * @return Collection&Concept[]
     */
    public function getConcepts(): Collection
    {
        return $this->concepts;
    }

    /**
     * @param Collection&Concept[] $concepts
     * @return $this
     */
    public function setConcepts(Collection $concepts)
    {
        $this->concepts = $concepts;
        return $this;
    }

    /**
     * @return Collection&Tax[]|null
     */
    public function getTaxes(): ?Collection
    {
        return $this->taxes;
    }

    /**
     * @param Collection&Tax[]|null $taxes
     * @return Cfdi
     */
    public function setTaxes(?Collection $taxes): Cfdi
    {
        $this->taxes = $taxes;
        return $this;
    }

    /**
     * @Serializer\PostDeserialize()
     */
    public function postDeserialize(): void
    {
        $this->version = (new CfdiVersionEnum(CfdiVersionEnum::V3_2()))->getValue();
    }
}
