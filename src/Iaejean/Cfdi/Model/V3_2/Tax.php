<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

class Tax
{
    /**
     * @Serializer\SerializedName("totalImpuestosTrasladados")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $totalTaxTransfers = null;

    /**
     * @Serializer\SerializedName("totalImpuestosRetenidos")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $totalTaxRetentions = null;

    /**
     * @var null|Collection&Transfer[]
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_2\Transfer>")
     * @Serializer\SerializedName("Traslados")
     * @Serializer\XmlElement(namespace="http://www.sat.gob.mx/cfd/3")
     * @Serializer\XmlList(inline=false, entry="Traslado", namespace="http://www.sat.gob.mx/cfd/3")
     */
    protected ?Collection $transfers = null;

    /**
     * @var null|Collection&Retention[]
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_2\Retention>")
     * @Serializer\SerializedName("Retenciones")
     * @Serializer\XmlElement(namespace="http://www.sat.gob.mx/cfd/3")
     * @Serializer\XmlList(inline=false, entry="Retencion", namespace="http://www.sat.gob.mx/cfd/3")
     */
    protected ?Collection $retentions = null;

    public function __construct()
    {
        $this->transfers = new ArrayCollection();
        $this->retentions = new ArrayCollection();
    }

    public function getTotalTaxTransfers(): ?float
    {
        return $this->totalTaxTransfers;
    }

    public function setTotalTaxTransfers(?float $totalTaxTransfers): Tax
    {
        $this->totalTaxTransfers = $totalTaxTransfers;
        return $this;
    }

    public function getTotalTaxRetentions(): ?float
    {
        return $this->totalTaxRetentions;
    }

    public function setTotalTaxRetentions(?float $totalTaxRetentions): Tax
    {
        $this->totalTaxRetentions = $totalTaxRetentions;
        return $this;
    }

    /**
     * @return null|Collection&Transfer[]
     */
    public function getTransfers(): ?Collection
    {
        return $this->transfers;
    }

    /**
     * @param null|Collection&Transfer[] $transfers
     * @return Tax
     */
    public function setTransfers(?Collection $transfers): Tax
    {
        $this->transfers = $transfers;
        return $this;
    }

    /**
     * @return null|Collection&Retention[]
     */
    public function getRetentions(): ?Collection
    {
        return $this->retentions;
    }

    /**
     * @param null|Collection&Retention[] $retentions
     * @return Tax
     */
    public function setRetentions(?Collection $retentions): Tax
    {
        $this->retentions = $retentions;
        return $this;
    }
}
