<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2;

use JMS\Serializer\Annotation as Serializer;

class CustomsInformation
{
    /**
     * @Serializer\SerializedName("numero")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $number;

    /**
     * @Serializer\SerializedName("fecha")
     * @Serializer\Type("DateTimeImmutable<'Y-m-d', 'America/Mexico_City'>")
     * @Serializer\XmlAttribute()
     */
    protected \DateTimeInterface $date;

    /**
     * @Serializer\SerializedName("aduana")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $customs = null;

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): CustomsInformation
    {
        $this->number = $number;
        return $this;
    }

    public function getDate(): \DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): CustomsInformation
    {
        $this->date = $date;
        return $this;
    }

    public function getCustoms(): ?string
    {
        return $this->customs;
    }

    public function setCustoms(?string $customs): CustomsInformation
    {
        $this->customs = $customs;
        return $this;
    }
}
