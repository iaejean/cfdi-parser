<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2;

use JMS\Serializer\Annotation as Serializer;

class PredialAccount
{
    /**
     * @Serializer\SerializedName("numero")
     * @Serializer\XmlAttribute()
     * @Serializer\Type("string")
     */
    protected string $number;

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): PredialAccount
    {
        $this->number = $number;
        return $this;
    }
}
