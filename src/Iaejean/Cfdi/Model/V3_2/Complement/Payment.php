<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2\Complement;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

class Payment
{
    /**
     * @Serializer\SerializedName("FechaPago")
     * @Serializer\Type("DateTimeImmutable<'Y-m-d\TH:i:s', 'America/Mexico_City'>")
     * @Serializer\XmlAttribute()
     */
    protected \DateTimeInterface $paymentDate;

    /**
     * @Serializer\SerializedName("FormaDePagoP")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $paymentWay;

    /**
     * @Serializer\SerializedName("MonedaP")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $currency;

    /**
     * @Serializer\SerializedName("TipoCambioP")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $exchangeRate = null;

    /**
     * @Serializer\SerializedName("Monto")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected float $amount;

    /**
     * @Serializer\SerializedName("NumOperacion")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $operationNumber = null;

    /**
     * @Serializer\SerializedName("RfcEmisorCtaOrd")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $emitterRfcOriginAccount = null;

    /**
     * @Serializer\SerializedName("NomBancoOrdExt")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $issuerBank = null;

    /**
     * @Serializer\SerializedName("CtaOrdenante")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $payerAccount = null;

    /**
     * @Serializer\SerializedName("RfcEmisorCtaBen")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $emitterRfcBeneficiaryAccount = null;

    /**
     * @Serializer\SerializedName("CtaBeneficiario")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $beneficiaryAccount = null;

    /**
     * @Serializer\SerializedName("TipoCadPago")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $paymentStringType = null;

    /**
     * @Serializer\SerializedName("CertPago")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $paymentCertificate = null;

    /**
     * @Serializer\SerializedName("CadPago")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $paymentString = null;

    /**
     * @Serializer\SerializedName("SelloPago")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $paymentSeal = null;

    /**
     * @var Collection&PaymentRelatedDocument[]|null
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_2\Complement\PaymentRelatedDocument>")
     * @Serializer\SerializedName("DoctoRelacionado")
     * @Serializer\XmlList(inline=true, entry="DoctoRelacionado", namespace="http://www.sat.gob.mx/Pagos")
     */
    protected ?Collection $paymentRelatedDocument;

    public function getPaymentDate(): \DateTimeInterface
    {
        return $this->paymentDate;
    }

    public function setPaymentDate(\DateTimeInterface $paymentDate): Payment
    {
        $this->paymentDate = $paymentDate;
        return $this;
    }

    public function getPaymentWay(): string
    {
        return $this->paymentWay;
    }

    public function setPaymentWay(string $paymentWay): Payment
    {
        $this->paymentWay = $paymentWay;
        return $this;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): Payment
    {
        $this->currency = $currency;
        return $this;
    }

    public function getExchangeRate(): ?float
    {
        return $this->exchangeRate;
    }

    public function setExchangeRate(?float $exchangeRate): Payment
    {
        $this->exchangeRate = $exchangeRate;
        return $this;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): Payment
    {
        $this->amount = $amount;
        return $this;
    }

    public function getOperationNumber(): ?string
    {
        return $this->operationNumber;
    }

    public function setOperationNumber(?string $operationNumber): Payment
    {
        $this->operationNumber = $operationNumber;
        return $this;
    }

    public function getEmitterRfcOriginAccount(): ?string
    {
        return $this->emitterRfcOriginAccount;
    }

    public function setEmitterRfcOriginAccount(?string $emitterRfcOriginAccount): Payment
    {
        $this->emitterRfcOriginAccount = $emitterRfcOriginAccount;
        return $this;
    }

    public function getIssuerBank(): ?string
    {
        return $this->issuerBank;
    }

    public function setIssuerBank(?string $issuerBank): Payment
    {
        $this->issuerBank = $issuerBank;
        return $this;
    }

    public function getPayerAccount(): ?string
    {
        return $this->payerAccount;
    }

    public function setPayerAccount(?string $payerAccount): Payment
    {
        $this->payerAccount = $payerAccount;
        return $this;
    }

    public function getEmitterRfcBeneficiaryAccount(): ?string
    {
        return $this->emitterRfcBeneficiaryAccount;
    }

    public function setEmitterRfcBeneficiaryAccount(?string $emitterRfcBeneficiaryAccount): Payment
    {
        $this->emitterRfcBeneficiaryAccount = $emitterRfcBeneficiaryAccount;
        return $this;
    }

    public function getBeneficiaryAccount(): ?string
    {
        return $this->beneficiaryAccount;
    }

    public function setBeneficiaryAccount(?string $beneficiaryAccount): Payment
    {
        $this->beneficiaryAccount = $beneficiaryAccount;
        return $this;
    }

    public function getPaymentStringType(): ?string
    {
        return $this->paymentStringType;
    }

    public function setPaymentStringType(?string $paymentStringType): Payment
    {
        $this->paymentStringType = $paymentStringType;
        return $this;
    }

    public function getPaymentCertificate(): ?string
    {
        return $this->paymentCertificate;
    }

    public function setPaymentCertificate(?string $paymentCertificate): Payment
    {
        $this->paymentCertificate = $paymentCertificate;
        return $this;
    }

    public function getPaymentString(): ?string
    {
        return $this->paymentString;
    }

    public function setPaymentString(?string $paymentString): Payment
    {
        $this->paymentString = $paymentString;
        return $this;
    }

    public function getPaymentSeal(): ?string
    {
        return $this->paymentSeal;
    }

    public function setPaymentSeal(?string $paymentSeal): Payment
    {
        $this->paymentSeal = $paymentSeal;
        return $this;
    }

    /**
     * @return Collection|PaymentRelatedDocument[]|null
     */
    public function getPaymentRelatedDocument()
    {
        return $this->paymentRelatedDocument;
    }

    public function setPaymentRelatedDocument($paymentRelatedDocument)
    {
        $this->paymentRelatedDocument = $paymentRelatedDocument;
        return $this;
    }
}
