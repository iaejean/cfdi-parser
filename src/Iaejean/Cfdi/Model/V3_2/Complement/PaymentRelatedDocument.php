<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2\Complement;

use JMS\Serializer\Annotation as Serializer;

class PaymentRelatedDocument
{
    /**
     * @Serializer\SerializedName("IdDocumento")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $documentId;

    /**
     * @Serializer\SerializedName("Serie")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $serie = null;

    /**
     * @Serializer\SerializedName("Folio")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $folio = null;

    /**
     * @Serializer\SerializedName("MonedaDR")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $currency;

    /**
     * @Serializer\SerializedName("TipoCambioDR")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $exchangeRate = null;

    /**
     * @Serializer\SerializedName("MetodoDePagoDR")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $paymentMethod = null;

    /**
     * @Serializer\SerializedName("NumParcialidad")
     * @Serializer\Type("int")
     * @Serializer\XmlAttribute()
     */
    protected ?int $partialityNumber = null;

    /**
     * @Serializer\SerializedName("ImpSaldoAnt")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $previousBalanceTax = null;

    /**
     * @Serializer\SerializedName("ImpPagado")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $taxPayed = null;

    /**
     * @Serializer\SerializedName("ImpSaldoInsoluto")
     * @Serializer\Type("float")
     * @Serializer\XmlAttribute()
     */
    protected ?float $unpaidBalanceTax = null;

    public function getDocumentId(): string
    {
        return $this->documentId;
    }

    public function setDocumentId(string $documentId): PaymentRelatedDocument
    {
        $this->documentId = $documentId;
        return $this;
    }

    public function getSerie(): ?string
    {
        return $this->serie;
    }

    public function setSerie(?string $serie): PaymentRelatedDocument
    {
        $this->serie = $serie;
        return $this;
    }

    public function getFolio(): ?string
    {
        return $this->folio;
    }

    public function setFolio(?string $folio): PaymentRelatedDocument
    {
        $this->folio = $folio;
        return $this;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): PaymentRelatedDocument
    {
        $this->currency = $currency;
        return $this;
    }

    public function getExchangeRate(): ?float
    {
        return $this->exchangeRate;
    }

    public function setExchangeRate(?float $exchangeRate): PaymentRelatedDocument
    {
        $this->exchangeRate = $exchangeRate;
        return $this;
    }

    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?string $paymentMethod): PaymentRelatedDocument
    {
        $this->paymentMethod = $paymentMethod;
        return $this;
    }

    public function getPartialityNumber(): ?int
    {
        return $this->partialityNumber;
    }

    public function setPartialityNumber(?int $partialityNumber): PaymentRelatedDocument
    {
        $this->partialityNumber = $partialityNumber;
        return $this;
    }

    public function getPreviousBalanceTax(): ?float
    {
        return $this->previousBalanceTax;
    }

    public function setPreviousBalanceTax(?float $previousBalanceTax): PaymentRelatedDocument
    {
        $this->previousBalanceTax = $previousBalanceTax;
        return $this;
    }

    public function getTaxPayed(): ?float
    {
        return $this->taxPayed;
    }

    public function setTaxPayed(?float $taxPayed): PaymentRelatedDocument
    {
        $this->taxPayed = $taxPayed;
        return $this;
    }

    public function getUnpaidBalanceTax(): ?float
    {
        return $this->unpaidBalanceTax;
    }

    public function setUnpaidBalanceTax(?float $unpaidBalanceTax): PaymentRelatedDocument
    {
        $this->unpaidBalanceTax = $unpaidBalanceTax;
        return $this;
    }
}
