<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2\Complement;

use JMS\Serializer\Annotation as Serializer;

class Tfd
{
    private const SCHEMA_LOCATION = 'http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/TimbreFiscalDigital/TimbreFiscalDigital.xsd';
    private const NAMESPACE = 'http://www.sat.gob.mx/TimbreFiscalDigital';

    /**
     * @Serializer\SerializedName("version")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $version;

    /**
     * @Serializer\SerializedName("UUID")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $uuid;

    /**
     * @Serializer\Type("DateTime<'Y-m-d\TH:i:s'>")
     * @Serializer\SerializedName("FechaTimbrado")
     * @Serializer\XmlAttribute()
     */
    protected ?\DateTimeInterface $stampDate = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("noCertificadoSAT")
     * @Serializer\XmlAttribute()
     */
    protected string $satCertificateNumber;

    /**
     * @Serializer\SerializedName("selloCFD")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $sealCFD;

    /**
     * @Serializer\SerializedName("selloSAT")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $sealSat;

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("xsi:schemaLocation")
     * @Serializer\XmlAttribute()
     */
    public function getSchemaLocation(): string
    {
        return self::SCHEMA_LOCATION;
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("xmlns:tfd")
     * @Serializer\XmlAttribute()
     */
    public function getNamespace(): string
    {
        return self::NAMESPACE;
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    public function setVersion(string $version): Tfd
    {
        $this->version = $version;
        return $this;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): Tfd
    {
        $this->uuid = $uuid;
        return $this;
    }

    public function getStampDate(): ?\DateTimeInterface
    {
        return $this->stampDate;
    }

    public function setStampDate(?\DateTimeImmutable $stampDate): Tfd
    {
        $this->stampDate = $stampDate;
        return $this;
    }

    public function getSatCertificateNumber(): string
    {
        return $this->satCertificateNumber;
    }

    public function setSatCertificateNumber(string $satCertificateNumber): Tfd
    {
        $this->satCertificateNumber = $satCertificateNumber;
        return $this;
    }

    public function getSealCFD(): string
    {
        return $this->sealCFD;
    }

    public function setSealCFD(string $sealCFD): Tfd
    {
        $this->sealCFD = $sealCFD;
        return $this;
    }

    public function getSealSat(): string
    {
        return $this->sealSat;
    }

    public function setSealSat(string $sealSat): Tfd
    {
        $this->sealSat = $sealSat;
        return $this;
    }
}
