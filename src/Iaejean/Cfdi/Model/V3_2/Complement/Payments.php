<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2\Complement;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

class Payments
{
    /**
     * @Serializer\SerializedName("Version")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $version;

    /**
     * @var Collection&Payment[]|null
     * @Serializer\SerializedName("Pago")
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_2\Complement\Payment>")
     * @Serializer\XmlList(inline=true, entry="Pago", namespace="http://www.sat.gob.mx/Pagos")
     */
    protected ?Collection $payment = null;

    public function getVersion(): string
    {
        return $this->version;
    }

    public function setVersion(string $version): Payments
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return Collection&Payment[]|null
     */
    public function getPayment(): ?Collection
    {
        return $this->payment;
    }

    /**
     * @param Collection&Payment[]|null $payment
     * @return $this
     */
    public function setPayment(?Collection $payment): Payments
    {
        $this->payment = $payment;
        return $this;
    }
}
