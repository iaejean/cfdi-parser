<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2;

use JMS\Serializer\Annotation as Serializer;

class Receiver
{
    /**
     * @Serializer\SerializedName("rfc")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $rfc;

    /**
     * @Serializer\SerializedName("nombre")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $name;

    /**
     * @Serializer\SerializedName("Domicilio")
     * @Serializer\Type("Iaejean\Cfdi\Model\V3_2\FiscalAddress")
     * @Serializer\XmlElement(namespace="http://www.sat.gob.mx/cfd/3")
     */
    protected ?FiscalAddress $fiscalAddress = null;

    public function getRfc(): string
    {
        return $this->rfc;
    }

    public function setRfc(string $rfc): Receiver
    {
        $this->rfc = $rfc;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Receiver
    {
        $this->name = $name;
        return $this;
    }

    public function getFiscalAddress(): ?FiscalAddress
    {
        return $this->fiscalAddress;
    }

    public function setFiscalAddress(?FiscalAddress $fiscalAddress): Receiver
    {
        $this->fiscalAddress = $fiscalAddress;
        return $this;
    }
}
