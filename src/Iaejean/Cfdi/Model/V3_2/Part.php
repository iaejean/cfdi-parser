<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

class Part
{
    /**
     * @Serializer\Type("float")
     * @Serializer\SerializedName("importe")
     * @Serializer\XmlAttribute()
     */
    protected ?float $amount = null;

    /**
     * @Serializer\Type("float")
     * @Serializer\SerializedName("valorUnitario")
     * @Serializer\XmlAttribute()
     */
    protected ?float $unitValue = null;

    /**
     * @Serializer\Type("int")
     * @Serializer\SerializedName("cantidad")
     * @Serializer\XmlAttribute()
     */
    protected ?int $quantity = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("descripcion")
     * @Serializer\XmlAttribute()
     */
    protected ?string $description = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("unidad")
     * @Serializer\XmlAttribute()
     */
    protected ?string $unit = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("noIdentificacion")
     * @Serializer\XmlAttribute()
     */
    protected ?string $identificationNumber = null;

    /**
     * @var Collection&CustomsInformation[]|null
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_2\CustomsInformation>")
     * @Serializer\SerializedName("InformacionAduanera")
     * @Serializer\XmlList(
     *     inline=true,
     *     entry="InformacionAduanera",
     *     namespace="http://www.sat.gob.mx/cfd/3",
     *     skipWhenEmpty=true
     * )
     */
    protected ?Collection $customsInformation = null;

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): Part
    {
        $this->amount = $amount;
        return $this;
    }

    public function getUnitValue(): ?float
    {
        return $this->unitValue;
    }

    public function setUnitValue(?float $unitValue): Part
    {
        $this->unitValue = $unitValue;
        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): Part
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Part
    {
        $this->description = $description;
        return $this;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit(?string $unit): Part
    {
        $this->unit = $unit;
        return $this;
    }

    public function getIdentificationNumber(): ?string
    {
        return $this->identificationNumber;
    }

    public function setIdentificationNumber(?string $identificationNumber): Part
    {
        $this->identificationNumber = $identificationNumber;
        return $this;
    }

    /**
     * @return Collection&CustomsInformation[]|null
     */
    public function getCustomsInformation(): ?Collection
    {
        return $this->customsInformation;
    }

    /**
     * @param Collection&CustomsInformation[]|null $customsInformation
     * @return Part
     */
    public function setCustomsInformation(?Collection $customsInformation)
    {
        $this->customsInformation = $customsInformation;
        return $this;
    }
}
