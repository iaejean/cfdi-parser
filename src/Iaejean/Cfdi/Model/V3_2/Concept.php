<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

class Concept
{
    /**
     * @Serializer\Type("float")
     * @Serializer\SerializedName("importe")
     * @Serializer\XmlAttribute()
     */
    protected ?float $amount = null;

    /**
     * @Serializer\Type("float")
     * @Serializer\SerializedName("valorUnitario")
     * @Serializer\XmlAttribute()
     */
    protected ?float $unitValue = null;

    /**
     * @Serializer\Type("int")
     * @Serializer\SerializedName("cantidad")
     * @Serializer\XmlAttribute()
     */
    protected ?int $quantity = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("descripcion")
     * @Serializer\XmlAttribute()
     */
    protected ?string $description = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("unidad")
     * @Serializer\XmlAttribute()
     */
    protected ?string $unit = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("noIdentificacion")
     * @Serializer\XmlAttribute()
     */
    protected ?string $identificationNumber = null;

    /**
     * @var Collection&CustomsInformation[]|null
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_2\CustomsInformation>")
     * @Serializer\SerializedName("InformacionAduanera")
     * @Serializer\XmlList(
     *     inline=true,
     *     entry="InformacionAduanera",
     *     namespace="http://www.sat.gob.mx/cfd/3",
     *     skipWhenEmpty=true
     * )
     */
    protected ?Collection $customsInformation = null;

    /**
     * @var Collection&PredialAccount[]|null
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_2\PredialAccount>")
     * @Serializer\SerializedName("CuentaPredial")
     * @Serializer\XmlList(
     *     inline=true,
     *     entry="CuentaPredial",
     *     namespace="http://www.sat.gob.mx/cfd/3",
     *     skipWhenEmpty=true
     * )
     */
    protected ?Collection $predialAccount = null;

    /**
     * @var Collection&Part[]|null
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_2\Part>")
     * @Serializer\SerializedName("Parte")
     * @Serializer\XmlList(inline=true, entry="Parte", namespace="http://www.sat.gob.mx/cfd/3", skipWhenEmpty=true)
     */
    protected ?Collection $part = null;

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): Concept
    {
        $this->amount = $amount;
        return $this;
    }

    public function getUnitValue(): ?float
    {
        return $this->unitValue;
    }

    public function setUnitValue(?float $unitValue): Concept
    {
        $this->unitValue = $unitValue;
        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): Concept
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Concept
    {
        $this->description = $description;
        return $this;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit(?string $unit): Concept
    {
        $this->unit = $unit;
        return $this;
    }

    public function getIdentificationNumber(): ?string
    {
        return $this->identificationNumber;
    }

    public function setIdentificationNumber(?string $identificationNumber): Concept
    {
        $this->identificationNumber = $identificationNumber;
        return $this;
    }

    /**
     * @return Collection&CustomsInformation[]|null
     */
    public function getCustomsInformation(): ?Collection
    {
        return $this->customsInformation;
    }

    /**
     * @param Collection&CustomsInformation[]|null $customsInformation
     * @return Concept
     */
    public function setCustomsInformation(?Collection $customsInformation)
    {
        $this->customsInformation = $customsInformation;
        return $this;
    }

    /**
     * @return Collection&PredialAccount[]|null
     */
    public function getPredialAccount(): ?Collection
    {
        return $this->predialAccount;
    }

    /**
     * @param Collection&PredialAccount[]|null $predialAccount
     * @return Concept
     */
    public function setPredialAccount(?Collection $predialAccount)
    {
        $this->predialAccount = $predialAccount;
        return $this;
    }

    /**
     * @return Collection&Part[]|null
     */
    public function getPart()
    {
        return $this->part;
    }

    /**
     * @param Collection&Part[]|null $part
     * @return Concept
     */
    public function setPart($part)
    {
        $this->part = $part;
        return $this;
    }
}
