<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2;

use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

class Emitter
{
    /**
     * @Serializer\SerializedName("rfc")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected string $rfc;

    /**
     * @Serializer\SerializedName("nombre")
     * @Serializer\Type("string")
     * @Serializer\XmlAttribute()
     */
    protected ?string $name = null;

    /**
     * @Serializer\SerializedName("DomicilioFiscal")
     * @Serializer\Type("Iaejean\Cfdi\Model\V3_2\FiscalAddress")
     * @Serializer\XmlElement(namespace="http://www.sat.gob.mx/cfd/3")
     */
    protected ?FiscalAddress $fiscalAddress = null;

    /**
     * @Serializer\SerializedName("ExpedidoEn")
     * @Serializer\Type("Iaejean\Cfdi\Model\V3_2\Issued")
     * @Serializer\XmlElement(namespace="http://www.sat.gob.mx/cfd/3")
     */
    protected ?Issued $issued = null;

    /**
     * @var Collection&FiscalRegime[]|null
     *
     * @Serializer\SerializedName("RegimenFiscal")
     * @Serializer\Type("ArrayCollection<Iaejean\Cfdi\Model\V3_2\FiscalRegime>")
     * Serializer\XmlElement(namespace="http://www.sat.gob.mx/cfd/3")
     * @Serializer\XmlList(inline=true, entry="RegimenFiscal", namespace="http://www.sat.gob.mx/cfd/3")
     */
    protected ?Collection $fiscalRegimes = null;

    public function getRfc(): string
    {
        return $this->rfc;
    }

    public function setRfc(string $rfc): Emitter
    {
        $this->rfc = $rfc;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): Emitter
    {
        $this->name = $name;
        return $this;
    }

    public function getFiscalAddress(): ?FiscalAddress
    {
        return $this->fiscalAddress;
    }

    public function setFiscalAddress(?FiscalAddress $fiscalAddress): Emitter
    {
        $this->fiscalAddress = $fiscalAddress;
        return $this;
    }

    public function getIssued(): ?Issued
    {
        return $this->issued;
    }

    public function setIssued(?Issued $issued): Emitter
    {
        $this->issued = $issued;
        return $this;
    }

    /**
     * @return FiscalRegime[]|Collection|null
     */
    public function getFiscalRegimes(): Collection
    {
        return $this->fiscalRegimes;
    }

    /**
     * @param FiscalRegime[]|Collection|null $fiscalRegimes
     * @return Emitter
     */
    public function setFiscalRegimes(Collection $fiscalRegimes)
    {
        $this->fiscalRegimes = $fiscalRegimes;
        return $this;
    }
}
