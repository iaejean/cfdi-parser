<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2;

use JMS\Serializer\Annotation as Serializer;

class Transfer
{
    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("impuesto")
     * @Serializer\XmlAttribute()
     */
    protected ?string $tax = null;

    /**
     * @Serializer\Type("float")
     * @Serializer\SerializedName("tasa")
     * @Serializer\XmlAttribute()
     */
    protected ?float $fee = null;

    /**
     * @Serializer\Type("float")
     * @Serializer\SerializedName("importe")
     * @Serializer\XmlAttribute()
     */
    protected ?float $amount = null;

    public function getTax(): ?string
    {
        return $this->tax;
    }

    public function setTax(?string $tax): Transfer
    {
        $this->tax = $tax;
        return $this;
    }

    public function getFee(): ?float
    {
        return $this->fee;
    }

    public function setFee(?float $fee): Transfer
    {
        $this->fee = $fee;
        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): Transfer
    {
        $this->amount = $amount;
        return $this;
    }
}
