<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2;

use JMS\Serializer\Annotation as Serializer;

class FiscalAddress
{
    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("codigoPostal")
     * @Serializer\XmlAttribute()
     */
    protected ?string $postalCode = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("localidad")
     * @Serializer\XmlAttribute()
     */
    protected ?string $locality = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("noExterior")
     * @Serializer\XmlAttribute()
     */
    protected ?string $outdoorNumber = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("estado")
     * @Serializer\XmlAttribute()
     */
    protected ?string $state = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("pais")
     * @Serializer\XmlAttribute()
     */
    protected ?string $country = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("municipio")
     * @Serializer\XmlAttribute()
     */
    protected ?string $municipality = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("colonia")
     * @Serializer\XmlAttribute()
     */
    protected ?string $suburb = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("calle")
     * @Serializer\XmlAttribute()
     */
    protected ?string $street = null;

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): FiscalAddress
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    public function getLocality(): ?string
    {
        return $this->locality;
    }

    public function setLocality(?string $locality): FiscalAddress
    {
        $this->locality = $locality;
        return $this;
    }

    public function getOutdoorNumber(): ?string
    {
        return $this->outdoorNumber;
    }

    public function setOutdoorNumber(?string $outdoorNumber): FiscalAddress
    {
        $this->outdoorNumber = $outdoorNumber;
        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): FiscalAddress
    {
        $this->state = $state;
        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): FiscalAddress
    {
        $this->country = $country;
        return $this;
    }

    public function getMunicipality(): ?string
    {
        return $this->municipality;
    }

    public function setMunicipality(?string $municipality): FiscalAddress
    {
        $this->municipality = $municipality;
        return $this;
    }

    public function getSuburb(): ?string
    {
        return $this->suburb;
    }

    public function setSuburb(?string $suburb): FiscalAddress
    {
        $this->suburb = $suburb;
        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): FiscalAddress
    {
        $this->street = $street;
        return $this;
    }
}
