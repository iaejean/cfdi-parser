<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2;

use JMS\Serializer\Annotation as Serializer;

class FiscalRegime
{
    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("Regimen")
     * @Serializer\XmlAttribute()
     */
    protected string $regime;

    public function getRegime(): string
    {
        return $this->regime;
    }

    public function setRegime(string $regime): FiscalRegime
    {
        $this->regime = $regime;
        return $this;
    }
}
