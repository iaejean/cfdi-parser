<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model\V3_2;

use Iaejean\Cfdi\Model\V3_2\Complement\Payments;
use Iaejean\Cfdi\Model\V3_2\Complement\Tfd;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\XmlNamespace("http://www.sat.gob.mx/TimbreFiscalDigital", prefix="tfd")
 * @Serializer\XmlNamespace("http://www.sat.gob.mx/Pagos", prefix="pago10")
 */
class Complement
{
    /**
     * @Serializer\Type("Iaejean\Cfdi\Model\V3_2\Complement\Tfd")
     * @Serializer\SerializedName("TimbreFiscalDigital")
     * @Serializer\XmlElement(namespace="http://www.sat.gob.mx/TimbreFiscalDigital")
     */
    protected Tfd $tfd;

    /**
     * @Serializer\Type("Iaejean\Cfdi\Model\V3_2\Complement\Payments")
     * @Serializer\SerializedName("Pagos")
     * @Serializer\XmlElement(namespace="http://www.sat.gob.mx/Pagos")
     */
    protected ?Payments $payments = null;

    public function getTfd(): Tfd
    {
        return $this->tfd;
    }

    public function setTfd(Tfd $tfd): Complement
    {
        $this->tfd = $tfd;
        return $this;
    }

    public function getPayments(): ?Payments
    {
        return $this->payments;
    }

    public function setPayments(?Payments $payments): Complement
    {
        $this->payments = $payments;
        return $this;
    }
}
