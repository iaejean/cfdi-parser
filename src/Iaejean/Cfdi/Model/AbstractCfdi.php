<?php

declare(strict_types=1);

namespace Iaejean\Cfdi\Model;

use JMS\Serializer\Annotation as Serializer;

/**
 * Serializer\Discriminator(
 *     field="Version",
 *     disabled=false,
 *     map={
 *      "3.3": "Iaejean\Cfdi\Model\V3_3\Cfdi"
 *     }
 * )
 * Serializer\XmlDiscriminator(attribute=true)
 */
abstract class AbstractCfdi
{
    abstract public function getVersion(): string;
}
