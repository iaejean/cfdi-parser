# CFDI Parser

[![pipeline status](https://gitlab.com/iaejean/cfdi-parser/badges/master/pipeline.svg)](https://gitlab.com/iaejean/cfdi-parser/commits/master)
[![coverage report](https://gitlab.com/iaejean/cfdi-parser/badges/master/coverage.svg)](https://gitlab.com/iaejean/cfdi-parser/commits/master)


### Gitlab Runner

* test: Will run PHP tools for quality standards, tests, reports and publish them
```shell script
$ gitlab-runner exec docker {STAGE_NAME} 
```
## Developers guide

### Contribution guidelines ###

Please consider read about some concepts like OOP, AOP, MVC, Patterns design, DDD, TDD, mutant Testing, PSR1, PSR2, etc.
 
This project has been provided with several tools for ensure the code quality:

* PHP Code Sniffer
* PHP Mess Detector
* PHP CS Fixer
* PHP Insights
```shell script
$ vendor/bin/phpcbf --standard=PSR1 src/ tests/
$ vendor/bin/phpcbf --standard=PSR2 src/ tests/

$ vendor/bin/phpmd src/ xml controversial design naming unusedcode --exclude=vendor/
$ vendor/bin/phpmd tests/ xml codesize controversial design naming unusedcode --exclude=vendor/

$ vendor/bin/php-cs-fixer fix src/ --dry-run --diff

$ vendor/bin/phpinsights --no-interaction
```

* PHPMetrics
* PHPLoc
* PHPStan
* Symfony Security Dependencies Checker
```shell script
$ vendor/bin/phploc src
$ vendor/bin/phpmetrics --report-html=report/metrics --junit=junit.xml ./
$ vendor/bin/phpstan analyse src tests
$ vendor/bin/security-checker security:check
```

* PHPUnit
* Infection
```shell script
$ vendor/bin/phpunit -c phpunit.xml.dist --coverage-text --colors=never

$ vendor/bin/infection
```
