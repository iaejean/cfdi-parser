<?php

declare(strict_types=1);

namespace Iaejean\Tests\Cfdi;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

trait TraitTest
{
    private static ?LoggerInterface $logger = null;

    public static function setupBeforeClass(): void
    {
        $level = (getenv('LOGGER_LEVEL')) ? getenv('LOGGER_LEVEL') : Logger::INFO;
        $logger = new Logger('test');
        $logger->pushHandler(new StreamHandler(__DIR__.'/../../logs/monolog.log', $level));
        self::$logger = $logger;
    }

    /**
     * @param object $object
     * @param string $methodName
     * @param array $params
     * @throws \ReflectionException
     * @return mixed
     */
    public function privateMethod(object $object, string $methodName, array $params = [])
    {
        $reflectionClass = new \ReflectionClass($object);
        $reflectionMethod = $reflectionClass->getMethod($methodName);
        $reflectionMethod->setAccessible(true);

        return $reflectionMethod->invokeArgs($object, $params);
    }
}
