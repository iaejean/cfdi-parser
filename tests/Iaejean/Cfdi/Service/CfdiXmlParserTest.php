<?php

declare(strict_types=1);

namespace Iaejean\Tests\Cfdi\Service;

use Iaejean\Cfdi\Contract\Service\CfdiXmlParserInterface;
use Iaejean\Cfdi\Exception\InvalidArgumentException;
use Iaejean\Cfdi\Model\AbstractCfdi;
use Iaejean\Cfdi\Service\CfdiXmlParser;
use Iaejean\Tests\Cfdi\TraitTest;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ValidatorBuilder;

class CfdiXmlParserTest extends TestCase
{
    use TraitTest {
        TraitTest::setupBeforeClass as traitSetupBeforeClass;
    }

    public static CfdiXmlParserInterface $cfdiXmlParser;

    public static function setupBeforeClass(): void
    {
        self::traitSetupBeforeClass();

        self::$cfdiXmlParser = new CfdiXmlParser(
            SerializerBuilder::create()->build(),
            (new ValidatorBuilder())->getValidator(),
            self::$logger
        );
    }

    public function testInvalidXml(): void
    {
        $this->expectException(InvalidArgumentException::class);
        self::$cfdiXmlParser->parseCfdi('fail');
    }

    public function testParseCfdi(): void
    {
        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi32-emitted.xml');
        $cfdi = self::$cfdiXmlParser->parseCfdi($xml);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi32.xml');
        $cfdi = self::$cfdiXmlParser->parseCfdi($xml);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi32_2.xml');
        $cfdi = self::$cfdiXmlParser->parseCfdi($xml);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi32_3.xml');
        $cfdi = self::$cfdiXmlParser->parseCfdi($xml);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33-emitted.xml');
        $cfdi = self::$cfdiXmlParser->parseCfdi($xml);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33-received.xml');
        $cfdi = self::$cfdiXmlParser->parseCfdi($xml);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33-emitted-concept.xml');
        $cfdi = self::$cfdiXmlParser->parseCfdi($xml);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33_payment_complement_doc_related.xml');
        $cfdi = self::$cfdiXmlParser->parseCfdi($xml);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33_payment_complement_stamped.xml');
        $cfdi = self::$cfdiXmlParser->parseCfdi($xml);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33_payment_complement_third_party.xml');
        $cfdi = self::$cfdiXmlParser->parseCfdi($xml);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);
    }

    public function testParseJson(): void
    {
        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi32-emitted.xml');
        $json = self::$cfdiXmlParser->parseJson($xml);
        self::assertJson($json);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi32.xml');
        $json = self::$cfdiXmlParser->parseJson($xml);
        self::assertJson($json);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi32_2.xml');
        $json = self::$cfdiXmlParser->parseJson($xml);
        self::assertJson($json);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi32_3.xml');
        $json = self::$cfdiXmlParser->parseJson($xml);
        self::assertJson($json);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33-emitted.xml');
        $json = self::$cfdiXmlParser->parseJson($xml);
        self::assertJson($json);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33-received.xml');
        $json = self::$cfdiXmlParser->parseJson($xml);
        self::assertJson($json);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33-emitted-concept.xml');
        $json = self::$cfdiXmlParser->parseJson($xml);
        self::assertJson($json);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33_payment_complement_doc_related.xml');
        $json = self::$cfdiXmlParser->parseJson($xml);
        self::assertJson($json);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33_payment_complement_stamped.xml');
        $json = self::$cfdiXmlParser->parseJson($xml);
        self::assertJson($json);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33_payment_complement_third_party.xml');
        $json = self::$cfdiXmlParser->parseJson($xml);
        self::assertJson($json);
    }

    public function testParseSimpleXml(): void
    {
        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi32-emitted.xml');
        $simpleXmlElement = self::$cfdiXmlParser->parseSimpleXmlElement($xml);
        self::assertInstanceOf(\SimpleXMLElement::class, $simpleXmlElement);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi32.xml');
        $simpleXmlElement = self::$cfdiXmlParser->parseSimpleXmlElement($xml);
        self::assertInstanceOf(\SimpleXMLElement::class, $simpleXmlElement);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi32_2.xml');
        $simpleXmlElement = self::$cfdiXmlParser->parseSimpleXmlElement($xml);
        self::assertInstanceOf(\SimpleXMLElement::class, $simpleXmlElement);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi32_3.xml');
        $simpleXmlElement = self::$cfdiXmlParser->parseSimpleXmlElement($xml);
        self::assertInstanceOf(\SimpleXMLElement::class, $simpleXmlElement);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33-emitted.xml');
        $simpleXmlElement = self::$cfdiXmlParser->parseSimpleXmlElement($xml);
        self::assertInstanceOf(\SimpleXMLElement::class, $simpleXmlElement);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33-received.xml');
        $simpleXmlElement = self::$cfdiXmlParser->parseSimpleXmlElement($xml);
        self::assertInstanceOf(\SimpleXMLElement::class, $simpleXmlElement);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33-emitted-concept.xml');
        $simpleXmlElement = self::$cfdiXmlParser->parseSimpleXmlElement($xml);
        self::assertInstanceOf(\SimpleXMLElement::class, $simpleXmlElement);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33_payment_complement_doc_related.xml');
        $simpleXmlElement = self::$cfdiXmlParser->parseSimpleXmlElement($xml);
        self::assertInstanceOf(\SimpleXMLElement::class, $simpleXmlElement);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33_payment_complement_stamped.xml');
        $simpleXmlElement = self::$cfdiXmlParser->parseSimpleXmlElement($xml);
        self::assertInstanceOf(\SimpleXMLElement::class, $simpleXmlElement);

        $xml = file_get_contents(__DIR__.'/../Fixtures/cfdi33_payment_complement_third_party.xml');
        $simpleXmlElement = self::$cfdiXmlParser->parseSimpleXmlElement($xml);
        self::assertInstanceOf(\SimpleXMLElement::class, $simpleXmlElement);
    }
}
