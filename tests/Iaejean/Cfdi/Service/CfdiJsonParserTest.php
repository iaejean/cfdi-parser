<?php

declare(strict_types=1);

namespace Iaejean\Tests\Cfdi\Service;

use Iaejean\Cfdi\Contract\Service\CfdiJsonParserInterface;
use Iaejean\Cfdi\Exception\InvalidArgumentException;
use Iaejean\Cfdi\Model\AbstractCfdi;
use Iaejean\Cfdi\Service\CfdiJsonParser;
use Iaejean\Tests\Cfdi\TraitTest;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ValidatorBuilder;

class CfdiJsonParserTest extends TestCase
{
    use TraitTest {
        TraitTest::setupBeforeClass as traitSetupBeforeClass;
    }

    public static CfdiJsonParserInterface $cfdiJsonParser;

    public static function setupBeforeClass(): void
    {
        self::traitSetupBeforeClass();

        self::$cfdiJsonParser = new CfdiJsonParser(
            SerializerBuilder::create()->build(),
            (new ValidatorBuilder())->getValidator(),
            self::$logger
        );
    }

    public function testInvalidJson(): void
    {
        $this->expectException(InvalidArgumentException::class);
        self::$cfdiJsonParser->parseCfdi('fail');
    }

    public function testParseCfdi(): void
    {
        $json = file_get_contents(__DIR__.'/../Fixtures/cfdi32-emitted.json');
        $cfdi = self::$cfdiJsonParser->parseCfdi($json);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $json = file_get_contents(__DIR__.'/../Fixtures/cfdi32.json');
        $cfdi = self::$cfdiJsonParser->parseCfdi($json);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $json = file_get_contents(__DIR__.'/../Fixtures/cfdi32_2.json');
        $cfdi = self::$cfdiJsonParser->parseCfdi($json);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $json = file_get_contents(__DIR__.'/../Fixtures/cfdi32_3.json');
        $cfdi = self::$cfdiJsonParser->parseCfdi($json);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $json = file_get_contents(__DIR__.'/../Fixtures/cfdi33-emitted.json');
        $cfdi = self::$cfdiJsonParser->parseCfdi($json);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $json = file_get_contents(__DIR__.'/../Fixtures/cfdi33-received.json');
        $cfdi = self::$cfdiJsonParser->parseCfdi($json);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);
    }

    public function testParseXml(): void
    {
        $json = file_get_contents(__DIR__.'/../Fixtures/cfdi32-emitted.json');
        $cfdi = self::$cfdiJsonParser->parseCfdi($json);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $json = file_get_contents(__DIR__.'/../Fixtures/cfdi32.json');
        $cfdi = self::$cfdiJsonParser->parseCfdi($json);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $json = file_get_contents(__DIR__.'/../Fixtures/cfdi32_2.json');
        $cfdi = self::$cfdiJsonParser->parseCfdi($json);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $json = file_get_contents(__DIR__.'/../Fixtures/cfdi32_3.json');
        $cfdi = self::$cfdiJsonParser->parseCfdi($json);
        self::assertInstanceOf(AbstractCfdi::class, $cfdi);

        $json = file_get_contents(__DIR__.'/../Fixtures/cfdi33-emitted.json');
        self::$cfdiJsonParser->parseXml($json);
        self::assertTrue(true);

        $json = file_get_contents(__DIR__.'/../Fixtures/cfdi33-received.json');
        self::$cfdiJsonParser->parseXml($json);
        self::assertTrue(true);
    }
}
