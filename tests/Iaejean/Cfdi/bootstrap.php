<?php

declare(strict_types=1);

if (!defined('PHPUNIT_COMPOSER_INSTALL')) {
    define('PHPUNIT_COMPOSER_INSTALL', __DIR__.'/../../../vendor/autoload.php');
}

if (!defined('CACHE_DIRECTORY')) {
    define('CACHE_DIRECTORY', __DIR__.'/../../../cache');
}

if (is_dir(CACHE_DIRECTORY)) {
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator(CACHE_DIRECTORY, RecursiveDirectoryIterator::SKIP_DOTS),
        RecursiveIteratorIterator::CHILD_FIRST
    );

    foreach ($files as $fileInfo) {
        $todo = ($fileInfo->isDir() ? 'rmdir' : 'unlink');
        $todo($fileInfo->getRealPath());
    }

    rmdir(CACHE_DIRECTORY);
}

mkdir(CACHE_DIRECTORY);
